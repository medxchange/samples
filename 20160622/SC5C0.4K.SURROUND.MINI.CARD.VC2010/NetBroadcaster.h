// NetBroadcaster.h : main header file for the NETBROADCASTER application
//

#if !defined(AFX_NETBROADCASTER_H__2C3E591A_E1FD_494E_9DAA_6E372149080B__INCLUDED_)
#define AFX_NETBROADCASTER_H__2C3E591A_E1FD_494E_9DAA_6E372149080B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CNetBroadcasterApp:
// See NetBroadcaster.cpp for the implementation of this class
//

class CNetBroadcasterApp : public CWinApp
{
public:
	CNetBroadcasterApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNetBroadcasterApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CNetBroadcasterApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NETBROADCASTER_H__2C3E591A_E1FD_494E_9DAA_6E372149080B__INCLUDED_)
