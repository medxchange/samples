; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CNetBroadcasterDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "NetBroadcaster.h"

ClassCount=3
Class1=CNetBroadcasterApp
Class2=CNetBroadcasterDlg
Class3=CAboutDlg

ResourceCount=5
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_NETBROADCASTER_DIALOG (English (U.S.))
Resource4=IDD_ABOUTBOX (English (U.S.))
Resource5=IDD_NETBROADCASTER_DIALOG

[CLS:CNetBroadcasterApp]
Type=0
HeaderFile=NetBroadcaster.h
ImplementationFile=NetBroadcaster.cpp
Filter=N

[CLS:CNetBroadcasterDlg]
Type=0
HeaderFile=NetBroadcasterDlg.h
ImplementationFile=NetBroadcasterDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CNetBroadcasterDlg

[CLS:CAboutDlg]
Type=0
HeaderFile=NetBroadcasterDlg.h
ImplementationFile=NetBroadcasterDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_NETBROADCASTER_DIALOG]
Type=1
Class=CNetBroadcasterDlg
ControlCount=8
Control1=IDC_STATIC_DISPLAY_WINDOW,static,1342177287
Control2=IDC_STATIC_FORMAT,static,1342308352
Control3=IDC_BUTTON_STOP,button,1342242816
Control4=IDC_BUTTON_START,button,1342242816
Control5=IDC_STATIC_HTTP_URL,static,1342308352
Control6=IDC_STATIC_RTMP_URL,static,1342308352
Control7=IDC_EDIT_RTMP,edit,1350639746
Control8=IDC_EDIT_HTTP,edit,1350639746

[DLG:IDD_NETBROADCASTER_DIALOG (English (U.S.))]
Type=1
Class=CNetBroadcasterDlg
ControlCount=8
Control1=IDC_STATIC_DISPLAY_WINDOW,static,1342177287
Control2=IDC_STATIC_FORMAT,static,1342308352
Control3=IDC_BUTTON_STOP,button,1342242816
Control4=IDC_BUTTON_START,button,1342242816
Control5=IDC_STATIC_HTTP_URL,static,1342308352
Control6=IDC_STATIC_RTMP_URL,static,1342308352
Control7=IDC_EDIT_RTMP,edit,1350639746
Control8=IDC_EDIT_HTTP,edit,1350639746

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

