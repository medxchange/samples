// NetBroadcasterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NetBroadcaster.h"
#include "NetBroadcasterDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

VOID SC_DEBUG( TCHAR * fmt, ... )
{
	TCHAR pszDebugDumpMessage[ 256 ] = "[QC] ";

	va_list marker;

	va_start( marker, fmt );

	vsprintf( pszDebugDumpMessage + 5, fmt, marker );

	va_end( marker );

	strcat( pszDebugDumpMessage, "\n" );

	OutputDebugString( pszDebugDumpMessage );
}

double PreviewFps = 0;

double PreviewStartTime = 0;

double PreviewDtime = 0;

ULONG PreviewFrameNum = 0;

CNetBroadcasterDlg * pNetBroadcasterDlg = NULL;

QRETURN on_no_signal_detected_callback( PVOID pDevice, ULONG nVideoInput, ULONG nAudioInput, PVOID pUserData )
{
	ULONG i = (ULONG)pUserData;

	if( i == 0)
	{
		EnterCriticalSection( &pNetBroadcasterDlg->m_hFpsAccessCriticalSection_p );

		PreviewFps = 0;

		PreviewStartTime = 0;

		PreviewDtime = 0;

		PreviewFrameNum = 0;

		LeaveCriticalSection( &pNetBroadcasterDlg->m_hFpsAccessCriticalSection_p );

		pNetBroadcasterDlg->m_statFormat.SetWindowText( "INFO: ..." );
	}
	else
	{
		pNetBroadcasterDlg->m_statFormatMini.SetWindowText( "INFO: ..." );
	}

	pNetBroadcasterDlg->m_nVideoWidth[ i ] = 0;
	
	pNetBroadcasterDlg->m_nVideoHeight[ i ] = 0;
	
	pNetBroadcasterDlg->m_bVideoIsInterleaved[ i ] = FALSE;
	
	pNetBroadcasterDlg->m_dVideoFrameRate[ i ] = 0.0;
	
	pNetBroadcasterDlg->m_nAudioChannels[ i ] = 0; 
	
	pNetBroadcasterDlg->m_nAudioBitsPerSample[ i ] = 0;
	
	pNetBroadcasterDlg->m_nAudioSampleFrequency[ i ] = 0;

	return QCAP_RT_OK;
}

QRETURN on_no_signal_removed_callback( PVOID pDevice, ULONG nVideoInput, ULONG nAudioInput, PVOID pUserData )
{
	ULONG i = (ULONG)pUserData;

	if( i == 0)
	{
		EnterCriticalSection( &pNetBroadcasterDlg->m_hFpsAccessCriticalSection_p );

		PreviewFps = 0;

		PreviewStartTime = 0;

		PreviewDtime = 0;

		PreviewFrameNum = 0;

		LeaveCriticalSection( &pNetBroadcasterDlg->m_hFpsAccessCriticalSection_p );

		pNetBroadcasterDlg->m_statFormat.SetWindowText( "INFO: ..." );
	}
	else
	{
		pNetBroadcasterDlg->m_statFormatMini.SetWindowText( "INFO: ..." );
	}

	pNetBroadcasterDlg->m_nVideoWidth[ i ] = 0;
	
	pNetBroadcasterDlg->m_nVideoHeight[ i ] = 0;
	
	pNetBroadcasterDlg->m_bVideoIsInterleaved[ i ] = FALSE;
	
	pNetBroadcasterDlg->m_dVideoFrameRate[ i ] = 0.0;
	
	pNetBroadcasterDlg->m_nAudioChannels[ i ] = 0; 
	
	pNetBroadcasterDlg->m_nAudioBitsPerSample[ i ] = 0;
	
	pNetBroadcasterDlg->m_nAudioSampleFrequency[ i ] = 0;

	return QCAP_RT_OK;
}    

QRETURN on_format_changed_callback( PVOID pDevice, ULONG nVideoInput, ULONG nAudioInput, ULONG nVideoWidth, ULONG nVideoHeight, BOOL bVideoIsInterleaved, double dVideoFrameRate, ULONG nAudioChannels, ULONG nAudioBitsPerSample, ULONG nAudioSampleFrequency, PVOID pUserData )
{
	ULONG i = (ULONG)pUserData;

	if( i == 0)
	{
		EnterCriticalSection( &pNetBroadcasterDlg->m_hFpsAccessCriticalSection_p );

		PreviewFps = 0;

		PreviewStartTime = 0;

		PreviewDtime = 0;

		PreviewFrameNum = 0;

		LeaveCriticalSection( &pNetBroadcasterDlg->m_hFpsAccessCriticalSection_p );

		CHAR psz[ MAX_PATH ];

		if( bVideoIsInterleaved == TRUE ) {

			sprintf( psz, "INFO: %dx%dI@%2.3fFPS, %dx%dBITSx%dHZ", nVideoWidth, nVideoHeight * 1, dVideoFrameRate, nAudioChannels, nAudioBitsPerSample, nAudioSampleFrequency );

			pNetBroadcasterDlg->m_statFormat.SetWindowText( psz );
		}
		else {

			sprintf( psz, "INFO: %dx%dP@%2.3fFPS, %dx%dBITSx%dHZ", nVideoWidth, nVideoHeight * 1, dVideoFrameRate, nAudioChannels, nAudioBitsPerSample, nAudioSampleFrequency );

			pNetBroadcasterDlg->m_statFormat.SetWindowText( psz );
		}
	}
	else
	{
		CHAR psz[ MAX_PATH ];

		if( bVideoIsInterleaved == TRUE ) {

			sprintf( psz, "INFO: %dx%dI@%2.3fFPS, %dx%dBITSx%dHZ", nVideoWidth, nVideoHeight * 1, dVideoFrameRate, nAudioChannels, nAudioBitsPerSample, nAudioSampleFrequency );

			pNetBroadcasterDlg->m_statFormatMini.SetWindowText( psz );
		}
		else {

			sprintf( psz, "INFO: %dx%dP@%2.3fFPS, %dx%dBITSx%dHZ", nVideoWidth, nVideoHeight * 1, dVideoFrameRate, nAudioChannels, nAudioBitsPerSample, nAudioSampleFrequency );

			pNetBroadcasterDlg->m_statFormatMini.SetWindowText( psz );
		}
	}

	SC_DEBUG("-------------on_format_changed_callback");

	pNetBroadcasterDlg->m_nVideoWidth[ i ] = nVideoWidth;
	
	pNetBroadcasterDlg->m_nVideoHeight[ i ] = nVideoHeight;
	
	pNetBroadcasterDlg->m_bVideoIsInterleaved[ i ] = bVideoIsInterleaved;
	
	pNetBroadcasterDlg->m_dVideoFrameRate[ i ] = dVideoFrameRate;
	
	pNetBroadcasterDlg->m_nAudioChannels[ i ] = nAudioChannels; 
	
	pNetBroadcasterDlg->m_nAudioBitsPerSample[ i ] = nAudioBitsPerSample;
	
	pNetBroadcasterDlg->m_nAudioSampleFrequency[ i ] = nAudioSampleFrequency;

	return QCAP_RT_OK;
}


QRETURN on_video_preview_callback( PVOID pDevice, double dSampleTime, BYTE * pFrameBuffer, ULONG nFrameBufferLen, PVOID pUserData )
{
	ULONG i = (ULONG)pUserData;

	if( i == 0)
	{
		EnterCriticalSection( &pNetBroadcasterDlg->m_hFpsAccessCriticalSection_p );

		SYSTEMTIME s_system_times; GetLocalTime( &s_system_times );

		ULONGLONG n_system_time = 0; SystemTimeToFileTime( &s_system_times, (FILETIME *)(&n_system_time) );

		double d_system_time = (ULONG)(n_system_time / 10000) / 1000.0;

		if( pFrameBuffer )
		{
			if(PreviewFrameNum++ != 0)
			{
				PreviewDtime = d_system_time;
			}
			else
			{
				PreviewStartTime = d_system_time;
			}
		}

		LeaveCriticalSection( &pNetBroadcasterDlg->m_hFpsAccessCriticalSection_p );

		static ULONG skip = 0;

		if(skip++ % 2)
		{
			return QCAP_RT_SKIP_DISPLAY;
		}
		else
		{
			return QCAP_RT_OK;
		}
	}
	else
	{
		return QCAP_RT_OK;
	}
}

// PREVIEW AUDIO CALLBACK FUNCTION
//
QRETURN on_audio_preview_callback( PVOID pDevice, double dSampleTime, BYTE* pFrameBuffer, ULONG nFrameBufferLen, PVOID pUserData )
{
    return QCAP_RT_OK;
}

extern CNetBroadcasterApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNetBroadcasterDlg dialog

CNetBroadcasterDlg::CNetBroadcasterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNetBroadcasterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNetBroadcasterDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CNetBroadcasterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNetBroadcasterDlg)
	DDX_Control(pDX, IDC_STATIC_FORMAT, m_statFormat);
	DDX_Control(pDX, IDC_STATIC_DISPLAY_WINDOW, m_statDisplayWindow);

	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_STATIC_DISPLAY_WINDOW2, m_statDisplayMiniWindow);
	DDX_Control(pDX, IDC_STATIC_FORMAT2, m_statFormatMini);
}

BEGIN_MESSAGE_MAP(CNetBroadcasterDlg, CDialog)
	//{{AFX_MSG_MAP(CNetBroadcasterDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_START, OnButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_STOP, OnButtonStop)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CNetBroadcasterDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON_START2, &CNetBroadcasterDlg::OnBnClickedButtonStart2)
	ON_BN_CLICKED(IDC_BUTTON_STOP2, &CNetBroadcasterDlg::OnBnClickedButtonStop2)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNetBroadcasterDlg message handlers

BOOL CNetBroadcasterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	// INITIALIZE COM LIBRARY
	//
	HRESULT hr = CoInitialize( NULL );

	pNetBroadcasterDlg = this;

	// INITIALIZE MEMBER VARIABLES
	//
	InitializeCriticalSection( &m_hFpsAccessCriticalSection_p );
	
	for( ULONG i = 0 ; i < 5 ; i++)
	{
		m_pDevice[ i ] = NULL;
	}

	for( ULONG i = 0 ; i < 2 ; i++)
	{
		m_nVideoWidth[ i ] = 0;
	
		m_nVideoHeight[ i ] = 0;
	
		m_bVideoIsInterleaved[ i ] = FALSE;
	
		m_dVideoFrameRate[ i ] = 0.0;
	
		m_nAudioChannels[ i ] = 0; 
	
		m_nAudioBitsPerSample[ i ] = 0;
	
		m_nAudioSampleFrequency[ i ] = 0;

		b_IsStartRecord[ i ] = false;
	}

	m_n4kDeviceNum = 1;

	// GET FIRST 4K DEVICE

	//ENUMERATION
	
	ULONGLONG * p_video_device_list = NULL;

	ULONGLONG * p_video_encoder_device_list = NULL;

	ULONGLONG * p_audio_device_list = NULL;

	ULONGLONG * p_audio_encoder_device_list = NULL;

	ULONG n_video_device_list_size = 0;

	ULONG n_video_encoder_device_list_size = 0;

	ULONG n_audio_device_list_size = 0;

	ULONG n_audio_encoder_device_list_size = 0;

	QCAP_DEVICE_ENUMERATION( &p_video_device_list, &n_video_device_list_size, &p_video_encoder_device_list, &n_video_encoder_device_list_size, &p_audio_device_list, &n_audio_device_list_size, &p_audio_encoder_device_list, &n_audio_encoder_device_list_size, QCAP_ENUM_TYPE_SERIAL_NUMBER);

	BOOL	b_IsFind4kDevice = false;

	for(ULONG i = 0 ; i < n_video_device_list_size ; i++)
	{
		p_video_device_list[i] = 0x000000000000FFFF & p_video_device_list[i];

		if( (p_video_device_list[i] == 0xf5cb) && (b_IsFind4kDevice == false))
		{
			m_n4kDeviceNum = i;

			b_IsFind4kDevice = true;
		}
	}


	GetDlgItem( IDC_BUTTON_START )->EnableWindow( TRUE );

	GetDlgItem( IDC_BUTTON_STOP )->EnableWindow( FALSE );

	GetDlgItem( IDC_BUTTON_START2 )->EnableWindow( TRUE );

	GetDlgItem( IDC_BUTTON_STOP2 )->EnableWindow( FALSE );

	////// INITIALIZE DEVICE RESOURCE
	//////
	QCAP_CREATE( "MZ0380 PCI", m_n4kDeviceNum + 0, m_statDisplayWindow.m_hWnd, &m_pDevice[ 0 ], TRUE );

	QCAP_CREATE( "MZ0380 PCI", m_n4kDeviceNum + 1, NULL, &m_pDevice[ 2 ], 1 );

	QCAP_CREATE( "MZ0380 PCI", m_n4kDeviceNum + 2, NULL, &m_pDevice[ 3 ], 1 );

	QCAP_CREATE( "MZ0380 PCI", m_n4kDeviceNum + 3, NULL, &m_pDevice[ 4 ], 1 );

	QCAP_SET_VIDEO_DEFAULT_OUTPUT_FORMAT(m_pDevice[ 0 ], QCAP_COLORSPACE_TYEP_YV12, 3840, 2160, 0, 60);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 0 ], 610, 4);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 2 ], 610, 1);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 3 ], 610, 1);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 4 ], 610, 1);


	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 0 ], 611, 0);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 2 ], 611, 1920);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 3 ], 611, 0);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 4 ], 611, 1920);


	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 0 ], 612, 0);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 2 ], 612, 0);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 3 ], 612, 1080);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 4 ], 612, 1080);
		
	//DISABLE SUB
	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 0 ], 273, 0);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 2 ], 273, 0);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 3 ], 273, 0);

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 4 ], 273, 0);

	// SET OUTPUT

	ULONG RESOLUTION = ( 3840 << 16 ) | ( 2160 << 0 );

	QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 0 ], 233, RESOLUTION);

	QCAP_REGISTER_FORMAT_CHANGED_CALLBACK( m_pDevice[ 0 ], on_format_changed_callback, (PVOID)0 );

	QCAP_REGISTER_NO_SIGNAL_DETECTED_CALLBACK( m_pDevice[ 0 ], on_no_signal_detected_callback, (PVOID)0 );

	QCAP_REGISTER_SIGNAL_REMOVED_CALLBACK( m_pDevice[ 0 ], on_no_signal_removed_callback, (PVOID)0 );

    QCAP_REGISTER_VIDEO_PREVIEW_CALLBACK( m_pDevice[ 0 ], on_video_preview_callback, (PVOID)0 );

    QCAP_REGISTER_AUDIO_PREVIEW_CALLBACK( m_pDevice[ 0 ], on_audio_preview_callback, (PVOID)0 );

	QCAP_SET_VIDEO_INPUT( m_pDevice[ 0 ], QCAP_INPUT_TYPE_AUTO );

	QCAP_SET_AUDIO_INPUT( m_pDevice[ 0 ], QCAP_INPUT_TYPE_EMBEDDED_AUDIO );

	QCAP_SET_AUDIO_VOLUME( m_pDevice[ 0 ], 0 );

	QCAP_RUN( m_pDevice[ 0 ] );

	if( m_n4kDeviceNum == 0 )
	{
		QCAP_CREATE( "MZ0380 PCI", m_n4kDeviceNum + 4, m_statDisplayMiniWindow.m_hWnd, &m_pDevice[ 1 ], TRUE );

		QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 1 ], 610, 0); // closed multi sync

		QCAP_REGISTER_FORMAT_CHANGED_CALLBACK( m_pDevice[ 1 ], on_format_changed_callback, (PVOID)1 );

		QCAP_REGISTER_NO_SIGNAL_DETECTED_CALLBACK( m_pDevice[ 1 ], on_no_signal_detected_callback, (PVOID)1 );

		QCAP_REGISTER_SIGNAL_REMOVED_CALLBACK( m_pDevice[ 1 ], on_no_signal_removed_callback, (PVOID)1 );

		QCAP_REGISTER_VIDEO_PREVIEW_CALLBACK( m_pDevice[ 1 ], on_video_preview_callback, (PVOID)1 );

		QCAP_REGISTER_AUDIO_PREVIEW_CALLBACK( m_pDevice[ 1 ], on_audio_preview_callback, (PVOID)1 );

		QCAP_SET_VIDEO_HARDWARE_ENCODER_PROPERTY( m_pDevice[ 1 ], 0, QCAP_ENCODER_FORMAT_H264, QCAP_RECORD_MODE_CBR, 8000, 12 * 1024 * 1024, 30, 0, 0, QCAP_DOWNSCALE_MODE_OFF, 0, 0);

		QCAP_SET_VIDEO_INPUT( m_pDevice[ 1 ], QCAP_INPUT_TYPE_AUTO );

		QCAP_SET_AUDIO_INPUT( m_pDevice[ 1 ], QCAP_INPUT_TYPE_EMBEDDED_AUDIO );

		QCAP_RUN( m_pDevice[ 1 ] );
	}
	else
	{
		QCAP_CREATE( "MZ0380 PCI", m_n4kDeviceNum - 1, m_statDisplayMiniWindow.m_hWnd, &m_pDevice[ 1 ], TRUE );

		QCAP_SET_DEVICE_CUSTOM_PROPERTY( m_pDevice[ 1 ], 610, 0); // closed multi sync

		QCAP_REGISTER_FORMAT_CHANGED_CALLBACK( m_pDevice[ 1 ], on_format_changed_callback, (PVOID)1 );

		QCAP_REGISTER_NO_SIGNAL_DETECTED_CALLBACK( m_pDevice[ 1 ], on_no_signal_detected_callback, (PVOID)1 );

		QCAP_REGISTER_SIGNAL_REMOVED_CALLBACK( m_pDevice[ 1 ], on_no_signal_removed_callback, (PVOID)1 );

		QCAP_REGISTER_VIDEO_PREVIEW_CALLBACK( m_pDevice[ 1 ], on_video_preview_callback, (PVOID)1 );

		QCAP_REGISTER_AUDIO_PREVIEW_CALLBACK( m_pDevice[ 1 ], on_audio_preview_callback, (PVOID)1 );

		QCAP_SET_VIDEO_HARDWARE_ENCODER_PROPERTY( m_pDevice[ 1 ], 0, QCAP_ENCODER_FORMAT_H264, QCAP_RECORD_MODE_CBR, 8000, 12 * 1024 * 1024, 30, 0, 0, QCAP_DOWNSCALE_MODE_OFF, 0, 0);

		QCAP_SET_VIDEO_INPUT( m_pDevice[ 1 ], QCAP_INPUT_TYPE_AUTO );

		QCAP_SET_AUDIO_INPUT( m_pDevice[ 1 ], QCAP_INPUT_TYPE_EMBEDDED_AUDIO );

		QCAP_RUN( m_pDevice[ 1 ] );
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CNetBroadcasterDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here

	OnButtonStop();

	OnBnClickedButtonStop2();

	for( ULONG i = 0 ; i < 5 ; i++)
	{
		if(m_pDevice[ i ])
		{
			QCAP_STOP( m_pDevice[ i ]);

			QCAP_DESTROY( m_pDevice[ i ]);

			m_pDevice[ i ] = NULL;
		}
	}

	DeleteCriticalSection( &m_hFpsAccessCriticalSection_p );

	// UNINITIALIZE COM LIBRARY
	// 
	CoUninitialize();
}

void CNetBroadcasterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CNetBroadcasterDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CNetBroadcasterDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CNetBroadcasterDlg::OnOK() 
{
	// TODO: Add extra validation here
	
//	CDialog::OnOK();
}

void CNetBroadcasterDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

void CNetBroadcasterDlg::OnButtonStart() 
{
	// TODO: Add your control notification handler code here

	OnButtonStop();

	if ( m_nVideoWidth[ 0 ] == 0 ) {

		return;
	}

	if ( m_nVideoHeight[ 0 ] == 0 ) {

		return;
	}

	QCAP_SET_AUDIO_RECORD_PROPERTY( m_pDevice[ 0 ], 2, QCAP_ENCODER_TYPE_SOFTWARE, QCAP_ENCODER_FORMAT_AAC );

	QCAP_SET_VIDEO_RECORD_PROPERTY_EX(m_pDevice[0], 2, QCAP_ENCODER_TYPE_INTEL_MEDIA_SDK, QCAP_ENCODER_FORMAT_H264, QCAP_RECORD_PROFILE_HIGH, QCAP_RECORD_LEVEL_41, QCAP_RECORD_ENTROPY_CABAC

		, QCAP_RECORD_COMPLEXITY_0, QCAP_RECORD_MODE_CBR, 8000, 120 * 1024 * 1024, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0);

	QCAP_START_RECORD( m_pDevice[ 0 ], 2, "RECORD.mp4" );			

	b_IsStartRecord[ 0 ] = true;

	GetDlgItem( IDC_BUTTON_START )->EnableWindow( FALSE );

	GetDlgItem( IDC_BUTTON_STOP )->EnableWindow( TRUE );
}

void CNetBroadcasterDlg::OnButtonStop() 
{
	// TODO: Add your control notification handler code here

	if ( b_IsStartRecord[ 0 ] == false )
	{
		return;
	}

	QCAP_STOP_RECORD( m_pDevice[ 0 ], 2 );
	

	GetDlgItem( IDC_BUTTON_START )->EnableWindow( TRUE );

	GetDlgItem( IDC_BUTTON_STOP )->EnableWindow( FALSE );
}


void CNetBroadcasterDlg::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here

	KillTimer( 0x00000001 );

	EnterCriticalSection( &m_hFpsAccessCriticalSection_p );

	PreviewFps = 0;

	PreviewStartTime = 0;

	PreviewDtime = 0;

	PreviewFrameNum = 0;

	LeaveCriticalSection( &m_hFpsAccessCriticalSection_p );

	SetTimer( 0x00000001, 5000, NULL );
}

void CNetBroadcasterDlg::OnTimer(UINT nIDEvent) 
{
	if ( nIDEvent == 0x00000001 ) {

		EnterCriticalSection( &m_hFpsAccessCriticalSection_p );

		if ( (m_nVideoWidth[ 0 ] != 0) && (m_nVideoHeight[ 0 ] != 0) )
		{
			PreviewFps = PreviewFrameNum/(PreviewDtime - PreviewStartTime);

			SC_DEBUG("pppppppppppp FRAME RATE = %f",PreviewFps);
		}

		LeaveCriticalSection( &m_hFpsAccessCriticalSection_p );

		CDialog::OnTimer(nIDEvent);
	}
}

void CNetBroadcasterDlg::OnBnClickedButtonStart2()
{
	// TODO: Add your control notification handler code here

	OnBnClickedButtonStop2();

	if ( m_nVideoWidth[ 1 ] == 0 ) {

		return;
	}

	if ( m_nVideoHeight[ 1 ] == 0 ) {

		return;
	}

	QCAP_SET_AUDIO_RECORD_PROPERTY( m_pDevice[ 1 ], 0, QCAP_ENCODER_TYPE_SOFTWARE, QCAP_ENCODER_FORMAT_AAC );

	QCAP_SET_VIDEO_HARDWARE_ENCODER_PROPERTY(m_pDevice[1], 0, QCAP_ENCODER_FORMAT_H264, QCAP_RECORD_MODE_CBR, 8000, 60 * 1024 * 1024, 30, 0, 0, QCAP_DOWNSCALE_MODE_OFF, 0, 0);

	QCAP_START_RECORD( m_pDevice[ 1 ], 0, "RECORD_MINI_CARD.mp4" );			

	b_IsStartRecord[ 1 ] = true;

	GetDlgItem( IDC_BUTTON_START2 )->EnableWindow( FALSE );

	GetDlgItem( IDC_BUTTON_STOP2 )->EnableWindow( TRUE );
}


void CNetBroadcasterDlg::OnBnClickedButtonStop2()
{
	// TODO: Add your control notification handler code here

	if ( b_IsStartRecord[ 1 ] == false )
	{
		return;
	}

	QCAP_STOP_RECORD( m_pDevice[ 1 ], 0 );

	GetDlgItem( IDC_BUTTON_START2 )->EnableWindow( TRUE );

	GetDlgItem( IDC_BUTTON_STOP2 )->EnableWindow( FALSE );
}
