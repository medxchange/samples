// NetBroadcasterDlg.h : header file
//

#include "afxwin.h"
#if !defined(AFX_NETBROADCASTERDLG_H__D4C5A09E_5B47_4DA6_BDA7_F83EFE2CE631__INCLUDED_)
#define AFX_NETBROADCASTERDLG_H__D4C5A09E_5B47_4DA6_BDA7_F83EFE2CE631__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CNetBroadcasterDlg dialog

class CNetBroadcasterDlg : public CDialog
{
// Construction
public:
	CNetBroadcasterDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CNetBroadcasterDlg)
	enum { IDD = IDD_NETBROADCASTER_DIALOG };
	CStatic	m_statFormat;
	CStatic m_statFormatMini;
	CStatic	m_statDisplayWindow;
	CStatic m_statDisplayMiniWindow;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNetBroadcasterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CNetBroadcasterDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnButtonStart();
	afx_msg void OnButtonStop();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButtonStart2();
	afx_msg void OnBnClickedButtonStop2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:

	PVOID				m_pDevice[ 5 ];

	ULONG				m_nVideoWidth[ 2 ];
	
	ULONG				m_nVideoHeight[ 2 ];
	
	BOOL				m_bVideoIsInterleaved[ 2 ];
	
	ULONG				m_dVideoFrameRate[ 2 ];
	
	ULONG				m_nAudioChannels[ 2 ]; 
	
	ULONG				m_nAudioBitsPerSample[ 2 ];
	
	ULONG				m_nAudioSampleFrequency[ 2 ];

	BOOL                b_IsStartRecord[ 2 ];

	UINT				m_n4kDeviceNum;

	CRITICAL_SECTION	m_hFpsAccessCriticalSection_p;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NETBROADCASTERDLG_H__D4C5A09E_5B47_4DA6_BDA7_F83EFE2CE631__INCLUDED_)
